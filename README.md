## a simple sprite sheet exporter for Blender

### spritesheet

<img src="./img/example.png" width="500"> 

### animation preview (gif)

<img src="./img/example.gif" width="500"> 
