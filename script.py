import bpy
import os
from math import radians
from mathutils import Vector
from mathutils import Euler
from PIL import Image

class Sprite:
    def __init__(self, scene, name, rotate_step, rotate_degree):
        self.scene = bpy.data.scenes[scene]
        self.object = self.scene.objects[name]
        self.start_frame = self.scene.frame_start
        self.end_frame = self.scene.frame_end
        self.num_of_frames = self.end_frame - self.start_frame + 1
        self.tile_x =  self.scene.render.resolution_x
        self.tile_y =  self.scene.render.resolution_y
        self.rotate_step = rotate_step
        self.rotate_degree = rotate_degree
        self.sheet = None
        self.gif = None

    def rotate_and_render(self, path):
        for d in range(self.rotate_step):
            self.object.rotation_euler[2] = radians(self.rotate_degree * d)
            i = 0
            for a in range(self.start_frame, self.end_frame + 1):
                self.scene.frame_current = a
                render_dir = bpy.path.abspath("//") + path +"/" + str(d) + "_" + str(i) #use scene if possible
                self.scene.render.filepath = render_dir
                bpy.ops.render.render(use_viewport=True, write_still=True) #use scene if possible
                i = i + 1

    def translate_nth_imgs(self, source_path, target_path, nth, x, y):
        for d in range(self.rotate_step):
            i = 0
            for a in range(self.start_frame, self.end_frame + 1):
                if i in nth:
                    img_path = bpy.path.abspath("//") + source_path + "/" + str(d) + "_" + str(i) + ".png"
                    img = Image.open(img_path)
                    tmp_img = Image.new("RGBA", (self.tile_x, self.tile_y))
                    tmp_img.paste(img, (x, -y))
                    tmp_img.save(bpy.path.abspath("//") + target_path + "/" + str(d) + "_" + str(i) + ".png")
                    #self.sheet.paste(image, (self.tile_x * r - x, self.tile_y * c - y))
                else:
                    print("else")
                i = i + 1

    def add_part(self, part_name, p_path, bg_path, target_path, row, x, y):
        for i in range(self.num_of_frames):
            part_path = bpy.path.abspath("//") + p_path + "/" + part_name + ".png"
            background_path = bpy.path.abspath("//") + bg_path + "/" + str(row) + "_" + str(i) + ".png"
            background = Image.open(background_path)
            part = Image.open(part_path)
            background.paste(part, (x, y), part)
            background.save(bpy.path.abspath("//") + target_path + "/" + str(row) + "_" + str(i) + ".png")
            
            

    def create_sheet(self, name, source_path, target_path):
        size = (self.tile_x * self.num_of_frames, self.tile_y * self.rotate_step)
        self.sheet = Image.new("RGBA", size)
        for c in range(self.rotate_step):
            for r in range(self.num_of_frames):  
                image_path = bpy.path.abspath("//") + source_path + "/" + str(c) + "_" + str(r) + ".png"
                image = Image.open(image_path)
                self.sheet.paste(image, (self.tile_x * r, self.tile_y * c))
        self.sheet.save(bpy.path.abspath("//") + target_path + "/" + name + ".png")
        

    def create_gif(self, name, source_path, target_path):
        size = (self.tile_x, self.tile_y)
        target_path = bpy.path.abspath("//") + target_path + "/"
        os.chdir(bpy.path.abspath("//") + "render")
        command = "convert -delay 0 -loop 0 -alpha set -dispose previous *.png " + target_path + name + ".gif"
        os.system(command)

    def reset(self):
        self.scene.frame_current = self.scene.frame_start
        self.object.rotation_euler[2] = radians(0)

# 

character = Sprite("Scene", "head", 8, 360/8)
character.rotate_and_render("render")
character.create_sheet("example", "render", "sprite_sheets")
character.create_gif("example", "render", "animations")

character.reset()








